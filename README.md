# TogglReport
---
Command line tool to generate toggl reports

# Installation

```
npm install togglreport -g
```

# Configuration

Add your auth-token

```
togglreport --auth
```

# Usage

```
togglreport --reportpath <path where reports should be saved>
```

## Optional paramters
1. -s, --start <start date (yyyy-mm-dd)> Start date for Toggl report summary.
2. -e, --end <end date (yyyy-mm-dd)> End date for Toggl report summary.
