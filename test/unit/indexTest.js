import TogglReportGenerator from '../../src/toggleReportGenerator';
import {assert} from 'chai';

describe('Basic Utils:', function() {
  beforeEach(() => {
    this.toggleReportGenerator = new TogglReportGenerator();
  });

  it('should give formatted date', () => {
    assert.match(this.toggleReportGenerator.getTodaysDate(), /\d{4}-\d{2}-\d{2}/);
  });

  it('reads authtoken from text file', () => {
    expect(this.toggleReportGenerator.getTogglAuthToken(__dirname + '/test-authtoken.txt')).to.equal('test-token');
  });

});
