#!/usr/bin/env node
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _toggleReportGenerator = require('./toggleReportGenerator');

var _toggleReportGenerator2 = _interopRequireDefault(_toggleReportGenerator);

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _prompt = require('prompt');

var _prompt2 = _interopRequireDefault(_prompt);

var _chalk = require('chalk');

var _chalk2 = _interopRequireDefault(_chalk);

var AUTH_TOKEN_FILE = __dirname + '/../authtoken.txt';

var authenticationProperties = [{
  name: 'authtoken',
  description: 'Your Toggl API authtoken',
  required: true
}];

_commander2['default'].version(0.9).description('Generate toggl report in PDF & JSON format for given start and end time.').option('-d, --reportpath <report path>', 'File system path at which the report will be stored.').option('-s, --start <start date (yyyy-mm-dd)>', 'Start date for Toggl report summary.').option('-e, --end <end date (yyyy-mm-dd)>', 'End date for Toggl report summary.').option('-a, --auth', 'Authenticate Toggl API.').parse(process.argv);

function updateAuthToken(result) {
  var authToken = result.authtoken;
  _fs2['default'].writeFileSync(AUTH_TOKEN_FILE, authToken);
}

function reportSuccess() {
  var output = {
    togglePdfReportPath: _commander2['default'].reportpath + '/summary.pdf',
    toggleJsonReportPath: _commander2['default'].reportpath + '/summary.json'
  };
  var pdfReportPath = output.togglePdfReportPath;
  var jsonReportPath = output.toggleJsonReportPath;
  console.log(_chalk2['default'].green('Report generated successfully in following paths\n'));

  var blueBold = _chalk2['default'].bold.blue;
  console.log(blueBold('PDF   : ') + _chalk2['default'].green(pdfReportPath));
  console.log(blueBold('JSON  : ') + _chalk2['default'].green(jsonReportPath));
}

function reportError() {
  var errorOutput = {
    error: 'failed to generate toggl reprot.'
  };
  console.log(errorOutput);
}

// Check if the request is of type authentication or API call
if (_commander2['default'].auth) {
  _prompt2['default'].start();
  _prompt2['default'].get(authenticationProperties, function (err, result) {
    if (err) {
      console.error('There was error in receiving inputs.');
    }
    updateAuthToken(result);
    process.exit(1);
  });
} else {
  if (!_commander2['default'].reportpath) {
    console.log(_chalk2['default'].red('Please provide --reportpath ' + '\nE.g. togglreport --reportpath /User/XYZ/Desktop/togglereports'));
    process.exit(1);
  }

  if (!_fs2['default'].lstatSync(_commander2['default'].reportpath).isDirectory()) {
    console.log(_chalk2['default'].red('Given path for storing reports is not a directory!'));
    process.exit(1);
  }
  new _toggleReportGenerator2['default'](_commander2['default']).generateTogglReport(reportSuccess, reportError);
}