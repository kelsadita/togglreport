Object.defineProperty(exports, '__esModule', {
  value: true
});

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _async = require('async');

var _async2 = _interopRequireDefault(_async);

var TogglReportGenerator = (function () {
  function TogglReportGenerator(cli) {
    _classCallCheck(this, TogglReportGenerator);

    var authToken = this.getTogglAuthToken(__dirname + '/../authtoken.txt');
    this.auth = 'Basic ' + new Buffer(authToken + ':api_token').toString('base64');

    var since = '&since=' + (cli.start || this.getTodaysDate());
    var until = '&until=' + (cli.end || this.getTodaysDate());
    var workspaceId = '&workspace_id=' + 765783;
    var other = '&display_hours=decimal&billable=no';
    var userAgent = '?user_agent=jarvis';
    var toggleReportBaseUrl = 'https://toggl.com/reports/api/v2';

    // PDF report URL
    var togglePdfReportBaseUrl = toggleReportBaseUrl + '/summary.pdf';
    this.togglPdfReportUrl = togglePdfReportBaseUrl + userAgent + workspaceId + since + until;

    // JSON report URL
    var toggleJsonReportBaseUrl = toggleReportBaseUrl + '/summary';
    this.togglJsonReportUrl = toggleJsonReportBaseUrl + userAgent + workspaceId + since + until + other;

    // Path for storing reports
    this.storePath = cli.reportpath;
  }

  _createClass(TogglReportGenerator, [{
    key: 'getReportRequests',
    value: function getReportRequests() {
      var jsonReportRequest = {
        type: 'json',
        url: this.togglJsonReportUrl,
        auth: this.auth,
        storePath: this.storePath
      };
      var pdfReportRequest = {
        type: 'pdf',
        url: this.togglPdfReportUrl,
        auth: this.auth,
        storePath: this.storePath
      };
      return [jsonReportRequest, pdfReportRequest];
    }
  }, {
    key: 'generateTogglReport',
    value: function generateTogglReport(reportSuccess, reportError) {
      _async2['default'].map(this.getReportRequests(), this.makeReportRequest, function (err, result) {
        if (err) {
          reportError(err);
        }
        reportSuccess(result);
      });
    }
  }, {
    key: 'makeReportRequest',
    value: function makeReportRequest(reportRequestObj, callback) {
      _request2['default'].get({
        url: reportRequestObj.url,
        headers: {
          'Authorization': reportRequestObj.auth
        }
      }).on('error', function (error) {
        callback(error);
      }).on('response', function (response) {
        callback(null, response);
      }).pipe(_fs2['default'].createWriteStream(reportRequestObj.storePath + '/summary.' + reportRequestObj.type));
    }
  }, {
    key: 'getTogglAuthToken',
    value: function getTogglAuthToken(authTokenFilePath) {
      return _fs2['default'].readFileSync(authTokenFilePath).toString().replace(/\n$/, '');
    }
  }, {
    key: 'getTodaysDate',
    value: function getTodaysDate() {
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth() + 1;
      var yyyy = today.getFullYear();
      if (dd < 10) {
        dd = '0' + dd;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      return yyyy + '-' + mm + '-' + dd;
    }
  }]);

  return TogglReportGenerator;
})();

exports['default'] = TogglReportGenerator;
module.exports = exports['default'];