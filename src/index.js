#!/usr/bin/env node

import TogglReportGenerator from './toggleReportGenerator';
import program from 'commander';
import fs from 'fs';
import prompt from 'prompt';
import chalk from 'chalk';

let AUTH_TOKEN_FILE = __dirname + '/../authtoken.txt';

let authenticationProperties = [{
  name: 'authtoken',
  description: 'Your Toggl API authtoken',
  required: true
}];

program
  .version(0.9)
  .description('Generate toggl report in PDF & JSON format for given start and end time.')
  .option('-d, --reportpath <report path>', 'File system path at which the report will be stored.')
  .option('-s, --start <start date (yyyy-mm-dd)>', 'Start date for Toggl report summary.')
  .option('-e, --end <end date (yyyy-mm-dd)>', 'End date for Toggl report summary.')
  .option('-a, --auth', 'Authenticate Toggl API.')
  .parse(process.argv);

function updateAuthToken(result) {
  let authToken = result.authtoken;
  fs.writeFileSync(AUTH_TOKEN_FILE, authToken);
}

function reportSuccess() {
  var output = {
    togglePdfReportPath: program.reportpath + '/summary.pdf',
    toggleJsonReportPath: program.reportpath + '/summary.json'
  };
  let pdfReportPath = output.togglePdfReportPath;
  let jsonReportPath = output.toggleJsonReportPath;
  console.log(chalk.green('Report generated successfully in following paths\n'));

  let blueBold = chalk.bold.blue;
  console.log(blueBold('PDF   : ') + chalk.green(pdfReportPath));
  console.log(blueBold('JSON  : ') + chalk.green(jsonReportPath));
}

function reportError() {
  var errorOutput = {
    error: 'failed to generate toggl reprot.'
  };
  console.log(errorOutput);
}

// Check if the request is of type authentication or API call
if (program.auth) {
  prompt.start();
  prompt.get(authenticationProperties, (err, result) => {
    if (err) {
      console.error('There was error in receiving inputs.');
    }
    updateAuthToken(result);
    process.exit(1);
  });
} else {
  if (!program.reportpath) {
    console.log(chalk.red('Please provide --reportpath ' +
    '\nE.g. togglreport --reportpath /User/XYZ/Desktop/togglereports'));
    process.exit(1);
  }

  if (!fs.lstatSync(program.reportpath).isDirectory()) {
    console.log(chalk.red('Given path for storing reports is not a directory!'));
    process.exit(1);
  }
  new TogglReportGenerator(program).generateTogglReport(reportSuccess, reportError);
}
