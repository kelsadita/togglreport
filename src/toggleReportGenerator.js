import request from 'request';
import fs from 'fs';
import async from 'async';

class TogglReportGenerator {
  constructor(cli) {

    var authToken           = this.getTogglAuthToken(__dirname + '/../authtoken.txt');
    this.auth               = 'Basic ' + new Buffer(authToken + ':api_token').toString('base64');

    var since               = '&since=' + (cli.start || this.getTodaysDate());
    var until               = '&until=' + (cli.end || this.getTodaysDate());
    var workspaceId         = '&workspace_id=' + 765783;
    var other               = '&display_hours=decimal&billable=no';
    var userAgent           = '?user_agent=jarvis';
    var toggleReportBaseUrl = 'https://toggl.com/reports/api/v2';

    // PDF report URL
    var togglePdfReportBaseUrl  = toggleReportBaseUrl + '/summary.pdf';
    this.togglPdfReportUrl  = togglePdfReportBaseUrl + userAgent + workspaceId + since + until;

    // JSON report URL
    var toggleJsonReportBaseUrl = toggleReportBaseUrl + '/summary';
    this.togglJsonReportUrl = toggleJsonReportBaseUrl + userAgent + workspaceId + since + until +
      other;

    // Path for storing reports
    this.storePath = cli.reportpath;
  }

  getReportRequests() {
    let jsonReportRequest = {
      type: 'json',
      url: this.togglJsonReportUrl,
      auth: this.auth,
      storePath: this.storePath
    };
    let pdfReportRequest = {
      type: 'pdf',
      url: this.togglPdfReportUrl,
      auth: this.auth,
      storePath: this.storePath
    };
    return [jsonReportRequest, pdfReportRequest];
  }

  generateTogglReport(reportSuccess, reportError) {
    async.map(this.getReportRequests(), this.makeReportRequest, function(err, result) {
      if (err) {
        reportError(err);
      }
      reportSuccess(result);
    });
  }

  makeReportRequest(reportRequestObj, callback) {
    request
    .get({
      url: reportRequestObj.url,
      headers: {
        'Authorization': reportRequestObj.auth
      }
    })
    .on('error', function(error) {
      callback(error);
    })
    .on('response', function(response) {
      callback(null, response);
    })
    .pipe(fs.createWriteStream(reportRequestObj.storePath + '/summary.' + reportRequestObj.type));
  }

  getTogglAuthToken(authTokenFilePath) {
    return fs.readFileSync(authTokenFilePath).toString().replace(/\n$/, '');
  }

  getTodaysDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
    }
    return yyyy + '-' + mm + '-' + dd;
  }
}

export default TogglReportGenerator;
